.extern __aeabi_ldivmod,__aeabi_uldivmod,__aeabi_d2lz,__aeabi_d2ulz,__aeabi_l2d,__aeabi_ul2d,__aeabi_f2lz,__aeabi_f2ulz,__aeabi_l2f,__aeabi_ul2f,__aeabi_d2f,__aeabi_d2iz,__aeabi_dadd,__aeabi_dcmpeq,__aeabi_dcmpge,__aeabi_dcmpgt,__aeabi_dcmple,__aeabi_dcmplt,__aeabi_ddiv,__aeabi_dmul,__aeabi_dsub,__aeabi_f2d,__aeabi_f2iz,__aeabi_fadd,__aeabi_fcmpeq,__aeabi_fcmpge,__aeabi_fcmpgt,__aeabi_fcmple,__aeabi_fcmplt,__aeabi_fdiv,__aeabi_fmul,__aeabi_fsub,__aeabi_i2d,__aeabi_i2f,__aeabi_idiv,__aeabi_ui2f
.text
.thumb

.global s_aeabi_ldivmod
.type s_aeabi_ldivmod,%function
.thumb_func
s_aeabi_ldivmod:
    b __aeabi_ldivmod

.global s_aeabi_uldivmod
.type s_aeabi_uldivmod,%function
.thumb_func
s_aeabi_uldivmod:
    b __aeabi_uldivmod

.global s_aeabi_d2lz
.type s_aeabi_d2lz,%function
.thumb_func
s_aeabi_d2lz:
    b __aeabi_d2lz

.global s_aeabi_d2ulz
.type s_aeabi_d2ulz,%function
.thumb_func
s_aeabi_d2ulz:
    b __aeabi_d2ulz

.global s_aeabi_l2d
.type s_aeabi_l2d,%function
.thumb_func
s_aeabi_l2d:
    b __aeabi_l2d

.global s_aeabi_ul2d
.type s_aeabi_ul2d,%function
.thumb_func
s_aeabi_ul2d:
    b __aeabi_ul2d

.global s_aeabi_f2lz
.type s_aeabi_f2lz,%function
.thumb_func
s_aeabi_f2lz:
    b __aeabi_f2lz

.global s_aeabi_f2ulz
.type s_aeabi_f2ulz,%function
.thumb_func
s_aeabi_f2ulz:
    b __aeabi_f2ulz

.global s_aeabi_l2f
.type s_aeabi_l2f,%function
.thumb_func
s_aeabi_l2f:
    b __aeabi_l2f

.global s_aeabi_ul2f
.type s_aeabi_ul2f,%function
.thumb_func
s_aeabi_ul2f:
    b __aeabi_ul2f

.global s_aeabi_d2f
.type s_aeabi_d2f,%function
.thumb_func
s_aeabi_d2f:
    b __aeabi_d2f

.global s_aeabi_d2iz
.type s_aeabi_d2iz,%function
.thumb_func
s_aeabi_d2iz:
    b __aeabi_d2iz

.global s_aeabi_dadd
.type s_aeabi_dadd,%function
.thumb_func
s_aeabi_dadd:
    b __aeabi_dadd

.global s_aeabi_dcmpeq
.type s_aeabi_dcmpeq,%function
.thumb_func
s_aeabi_dcmpeq:
    b __aeabi_dcmpeq

.global s_aeabi_dcmpge
.type s_aeabi_dcmpge,%function
.thumb_func
s_aeabi_dcmpge:
    b __aeabi_dcmpge

.global s_aeabi_dcmpgt
.type s_aeabi_dcmpgt,%function
.thumb_func
s_aeabi_dcmpgt:
    b __aeabi_dcmpgt

.global s_aeabi_dcmple
.type s_aeabi_dcmple,%function
.thumb_func
s_aeabi_dcmple:
    b __aeabi_dcmple

.global s_aeabi_dcmplt
.type s_aeabi_dcmplt,%function
.thumb_func
s_aeabi_dcmplt:
    b __aeabi_dcmplt

.global s_aeabi_ddiv
.type s_aeabi_ddiv,%function
.thumb_func
s_aeabi_ddiv:
    b __aeabi_ddiv

.global s_aeabi_dmul
.type s_aeabi_dmul,%function
.thumb_func
s_aeabi_dmul:
    b __aeabi_dmul

.global s_aeabi_dsub
.type s_aeabi_dsub,%function
.thumb_func
s_aeabi_dsub:
    b __aeabi_dsub

.global s_aeabi_f2d
.type s_aeabi_f2d,%function
.thumb_func
s_aeabi_f2d:
    b __aeabi_f2d

.global s_aeabi_f2iz
.type s_aeabi_f2iz,%function
.thumb_func
s_aeabi_f2iz:
    b __aeabi_f2iz

.global s_aeabi_fadd
.type s_aeabi_fadd,%function
.thumb_func
s_aeabi_fadd:
    b __aeabi_fadd

.global s_aeabi_fcmpeq
.type s_aeabi_fcmpeq,%function
.thumb_func
s_aeabi_fcmpeq:
    b __aeabi_fcmpeq

.global s_aeabi_fcmpge
.type s_aeabi_fcmpge,%function
.thumb_func
s_aeabi_fcmpge:
    b __aeabi_fcmpge

.global s_aeabi_fcmpgt
.type s_aeabi_fcmpgt,%function
.thumb_func
s_aeabi_fcmpgt:
    b __aeabi_fcmpgt

.global s_aeabi_fcmple
.type s_aeabi_fcmple,%function
.thumb_func
s_aeabi_fcmple:
    b __aeabi_fcmple

.global s_aeabi_fcmplt
.type s_aeabi_fcmplt,%function
.thumb_func
s_aeabi_fcmplt:
    b __aeabi_fcmplt

.global s_aeabi_fdiv
.type s_aeabi_fdiv,%function
.thumb_func
s_aeabi_fdiv:
    b __aeabi_fdiv

.global s_aeabi_fmul
.type s_aeabi_fmul,%function
.thumb_func
s_aeabi_fmul:
    b __aeabi_fmul

.global s_aeabi_fsub
.type s_aeabi_fsub,%function
.thumb_func
s_aeabi_fsub:
    b __aeabi_fsub

.global s_aeabi_i2d
.type s_aeabi_i2d,%function
.thumb_func
s_aeabi_i2d:
    b __aeabi_i2d

.global s_aeabi_i2f
.type s_aeabi_i2f,%function
.thumb_func
s_aeabi_i2f:
    b __aeabi_ui2f

.global s_aeabi_idiv
.type s_aeabi_idiv,%function
.thumb_func
s_aeabi_idiv:
    b __aeabi_idiv

.global s_aeabi_ui2f
.type s_aeabi_ui2f,%function
.thumb_func
s_aeabi_ui2f:
    b __aeabi_ui2f
